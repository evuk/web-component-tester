# web-component-tester

Docker image for testing [Polymer](https://www.polymer-project.org/) based web components in
[Bitbucket](https://bitbucket.org) pipelines. Based on [pascallaier/web-component-tester
](https://hub.docker.com/r/pascallaier/web-component-tester/dockerfile).
