FROM selenium/standalone-chrome

MAINTAINER EValue Developers <developers@ev.uk>

RUN sudo apt-get update
RUN sudo apt-get install -y curl xvfb git
RUN curl -sL https://deb.nodesource.com/setup_9.x | sudo -E bash -
RUN sudo apt-get install -y nodejs
RUN sudo npm install -g bower polymer-cli
RUN sudo chown -R seluser:seluser ~/.config
RUN sudo chown -R seluser:seluser ~/.npm
